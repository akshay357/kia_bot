var debug = require('debug')('botkit:onboarding');

module.exports = function (controller) {

    controller.on('onboard', function (bot) {

        debug('Starting an onboarding experience!');

        bot.startPrivateConversation({user: bot.config.createdBy}, function (err, talk) {
            if (err) {
                console.log(err);
            } else {
                talk.say('I am a bot that has just joined your workspace!');
            }
        });
    });

}
