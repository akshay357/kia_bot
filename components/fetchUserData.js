var fs = require('fs');
var csv = require('csv');
var path = require('path');
var async = require('async');

var obj = csv();

var colorGradient = ["#051937", "#004d7a", "#008793", "#00bf72", "#a8eb12"];
var attachments = [];

var sentence = ["Here are some people that may help you with $var",
"There are some of your colleagues that might help you in $var",
"These employees at AMEX are proficient in $var, they will guide you",
"People skilled at $var will help you solve your problem"];

var technologies = ["javascript", "python", "java"];

function compareUserProficientRating(a, b){

    if(a.pr > b.pr) return -1;
    else return 1;
}

function compareUserRichScore(a, b){

    if(a.rich > b.rich) return -1;
    else return 1;
}

var users = {
    python:[],
    java:[],
    javascript:[]
};

module.exports = {

    fetchData: function(technology, numberOfPeople, callback){
        // var name = ["Akshay", "Kashish", "Mayank", "Chhavi", "Himanshu"];
        // var email = ["akshay.maheshwari@aexp.com", "kashish.malhotra@aexp.com", "mayank.goel1@aexp.com", "chhavi.chauhan1@aexp.com", "himanshu.gupta3@aexp.com"];
        // var rating = ["1600", "1402", "1853", "1832", "1330"];

        var index;

        // console.log("in fetch data", technology);
        async.waterfall([
            function(callback2){

                if(users.python.length == 0){
                    obj.from.path('/home/akhem301/Downloads/slack_starter_chatbot/components/kia_usr_metrics_final.csv').to.array(function (data) {
                        for (index = 1; index < data.length; index++) {
                            users[data[index][9]].push({
                                name: data[index][0],
                                email: data[index][1],
                                rich: Math.round(parseFloat(data[index][7])),
                                pr: Math.round(parseFloat(data[index][8])),
                                tech: data[index][9],
                            });
                        }

                        for(var i = 0; i < technologies.length; i += 1){
                            users[technologies[i]].sort(compareUserProficientRating);
                        }
                        // console.log(users.slice(0, numberOfPeople));

                        if(index == data.length){
                            // console.log(users.slice(0, numberOfPeople));
                            callback2(null, users[technology].slice(0, numberOfPeople));
                        }
                    });
                }
                else {
                    // console.log(users);
                    callback2(null, users[technology].slice(0, numberOfPeople));
                }
            },
        ], function (err,result) {
            // console.log(result)
            var finalResult = {
                tech: technology,
                users: result
            };
            callback(null, finalResult);
        });
        // return users.slice(0, numberOfPeople);
    },

    createData: function(dataObject){
        var attachments = [];
        // console.log(dataObject);
        for(var i = 0; i < dataObject.users.length; i += 1){
            // console.log(dataObject[i].name);
            var object = {
                color: colorGradient[i%(colorGradient.length)],
                callback_id: 'user',
                attachment_type: 'default',
                actions: [
                    {
                        name: "skype_message_button",
                        type: "button",
                        text: "Message on Skype",
                        value: dataObject.users[i].email
                    },
                    {
                        name: "email_message_button",
                        type: "button",
                        text: "Email :email:",
                        value: dataObject.users[i].email
                    }
                ],
                // pretext: "Optional text that appears above the attachment block",
                author_name: dataObject.users[i].name +"\t("+dataObject.users[i].email+")",
                author_link: dataObject.users[i].email,
                author_icon: "http://flickr.com/icons/bobby.jpg",
                title: "Proficient Rating \t RICH Score",
                text: "`"+dataObject.users[i].pr+"`" + "\t\t\t\t\t\t\t" + "`"+dataObject.users[i].rich+"`"
            };
            // object.callback_id = object.callback_id+i;
            attachments.push(object);
        }

        // console.log(dataObject);

        var rnum = Math.floor(Math.random()*10);
        var templatedMessage = sentence[rnum % sentence.length];
        templatedMessage = templatedMessage.replace("$var","`"+dataObject.tech+"`");

        var finalJson = {
            text: templatedMessage,
            attachments: attachments
        };

        return finalJson;
    },
    readMessageAndConvert: function (message, callback_main){

        var technology=null;
        message = message.event.text.toLowerCase();
        for(var i = 0; i < technologies.length; i += 1){
            if(message.includes(technologies[i])){
                technology = technologies[i];
                break;
            }
        }

        if(technology != null){
            async.waterfall([
                function(callback){
                    module.exports.fetchData(technology, 5, function(err, data){
                        // console.log("hello here", data);
                        callback(null, data);
                    });
                },
                function(dataPrevious,callback){
                    // console.log(data);
                    var formattedMessage = module.exports.createData(dataPrevious);
                    callback(null, formattedMessage);
                },
            ], function (err,result) {
                // console.log(result)
                callback_main(null, result);
            });
        }
        else {
            callback_main(null, null);
        }
    }
}
