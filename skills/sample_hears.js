var opn = require('opn');
var allData = require('../components/fetchUserData');
var async = require('async');
var flag=false;

module.exports = function(controller) {

    controller.on('direct_message', function(bot, message) {

        allData.readMessageAndConvert(message, function(err, replyMessage){
            if(replyMessage!=null){
                console.log(replyMessage);
                bot.reply(message, replyMessage);
            }
            else {
                console.log(replyMessage);
                bot.reply(message, {
                    text:"I did not get what you just said, but I can help you find people who may guide you",
                    attachments: [
                        {
                            text: "Do you want to proceed?",
                            callback_id: "default",
                            color: "#f4424b",
                            attachment_type: "default",
                            actions: [
                                {
                                    "name": "yes",
                                    "text": "Sure!",
                                    "type": "button",
                                    "value": "yes"
                                },
                                {
                                    "name": "no",
                                    "text": "Not really",
                                    "type": "button",
                                    "value": "no"
                                }
                            ]
                        }
                    ]
                });
            }
        });
    });

    // receive an interactive message, and reply with a message that will replace the original
    controller.on('interactive_message_callback', function(bot, message) {
        // check message.actions and message.callback_id to see what action to take...
        if(message.callback_id == "default"){
            if(message.actions[0].value == 'yes'){

                bot.replyInteractive(message, {
                    text: 'I can help you find people who may help you in the following',
                    attachments: [
                        {
                            title: 'Technologies to select from',
                            callback_id: 'techOption',
                            color: "#3AA3E3",
                            attachment_type: 'default',
                            actions: [
                                {
                                    name: 'technologies_list',
                                    text: 'Choose a technology',
                                    type: 'select',
                                    options: [
                                        {
                                            "text": "Java",
                                            "value": "java"
                                        },
                                        {
                                            "text": "Python",
                                            "value": "python"
                                        },
                                        {
                                            "text": "Javascript",
                                            "value": "javascript"
                                        },
                                    ]
                                }
                            ]
                        }
                    ]
                });
            }
            else {
                bot.replyInteractive(message, {text: 'Not a problem, Do let me know when you need me :)'});
            }
        }
        else if(message.callback_id == 'techOption'){
            // console.log(message.actions[0].selected_options[0].value);
            var technology = message.actions[0].selected_options[0].value;

            // function call
            async.waterfall([
                function(callback){
                    allData.fetchData(technology, 5, function(err, data){
                        // console.log("hello here", data);
                        callback(null, data);
                    });
                },
                function(dataPrevious,callback){
                    // console.log(data);
                    var formattedMessage = allData.createData(dataPrevious);
                    callback(null, formattedMessage);
                },
            ], function (err,result) {
                // console.log(result)
                bot.reply(message, result);
            });
            // End
        }
        else if(message.callback_id == 'user'){
            if(message.actions[0].name == 'skype_message_button'){

            }
            else {

            }
        }
    });
};
